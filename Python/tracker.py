"""

Example: tracker.py

Demonstrates:
 - Display (animation)
 - Compass
 - Speaker/Music
 - Pseudo random generator

This example acts like a pseduo tracker by selecting a random direction, then
showing an animation slower or faster depending on how close you are to the
direction.

"""

# Importing bare essentials to save memory

from microbit import display, compass, Image
from random import randrange
from music import play


# Five frame tracker animation

tracker_display = [
Image("00000:"
"00000:"
"00000:"
"00000:"
"00900:"),
Image("00000:"
"00000:"
"00000:"
"00700:"
"07970:"),
Image("00000:"
"00000:"
"00700:"
"07070:"
"70907:"),
Image("00000:"
"00700:"
"07070:"
"70007:"
"00900:"),
Image("00700:"
"07070:"
"70007:"
"00000:"
"00900:")
]

# Init

# Compass needs to be calibrated before running
# Done by drawing a circle by tilting the device

compass.calibrate()

#Selecting the direction to go nuts
random_direction = randrange(0, 11)

while True:

    play("C4:1", wait=False) # Play a note asyncronsly

    # Running the animation
    display.show(tracker_display, delay=400-compass.heading())
