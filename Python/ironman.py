"""

example: ironman.py

Demonstrates:
 - Music
 - Neopixel
 - Speech

"""

from microbit import pin1, sleep
from music import play, set_tempo
from neopixel import NeoPixel
from speech import say

# Iron man music
ironman = [
"B:4", "D5:4", "D5:2", "E5:2", "E5:4",
"G5:1","F#:1" ,"G5:1", "F#:1", "G5:2", "D5", "D5", "E5:2", "E5:2"
]


np_pin = pin1 # The pin where neopixel is connected, 0 is meant for music
np_count = 16 # The count of neopixels
np = NeoPixel(pin1, np_count) # Initiliaze neopixel
np.clear()


def spin_pixels(pixels, wait=200):
    """ Creates a spinning effect.

    Lights up entire neopixel by enabling 1 LED at a time.
    """
    for pixel in range(0,pixels):
        np.clear()
        np[pixel] = (0,0,255)
        np.show()
        sleep(wait)


spin_pixels(np_count, 100) # Spinning the LEDS
np.clear() # Clearing the screen
say("I am iron man!", pitch=100, speed=150) # Shits and giggles

# Alternate pixel

for pixel in range(0, np_count):

    if not pixel%2:
        np[pixel] = (0,0,255)

np.show()



# Play music
set_tempo(bpm=72, ticks=4)
play(ironman)
