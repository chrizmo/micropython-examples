"""
Example: temp.py

Demonstrates:
 - Temperature
 - UART/Serial transmission
 - Display

Uses the internal temprature of microbit and displays it. Transmits exact temprature over UART

"""

from microbit import uart, button_a, button_b, temperature, sleep, display, Image

uart.init(19200)

def create_temperature_image(temp):
    """ Creates a temperature image in the range -40/40.
    One dot represents 20c
    """

    display_temp = Image("00900:"
    "00900:"
    "00900:"
    "00900:"
    "00900:")

    # each dot represent 50%

    # temp / 40 * 100

while True:
    temp = temperature()
    # display.show(create_temperature_image(temp))
    display.show(str(temp))
    uart.write(str(temp))
    sleep(4000)
