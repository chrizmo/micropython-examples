"""
Example: radio.py

Demonstrates
 - Radio
 - Display
 - Buttons

 This example will let you send some images to another microbit.

 One microbit is set up as a sender, the other just receieves. When you switch
 image on sender, it's updated automatically on the recever.

"""

from microbit import button_a, button_b, display, Image, sleep
import radio

function = "sender" # Default function
user_selection = 0 # The selection a user made
incoming = 0 # The incoming integer, only used if microbit is receiver
last_sent = 0 # The last sent image. Only used to detect changes in selection


# Only a subset of the images, just to test the functions
images = [
    Image.SURPRISED,
    Image.HEART,
    Image.CHESSBOARD,
    Image.COW,
    Image.GHOST,
    Image.SWORD
]

# Set functional mode: sender and receiver. Mode is changed with A and selected
# with B

while(not button_b.is_pressed()):
    if(function == "sender"):
        # Mode is sender
        display.show("S")
    else:
        # Mode is receiver
        display.show("R")

    if(button_a.was_pressed()):
        if(function == "sender"):
            function = "receiver"
        else:
            function = "sender"

# Displaying the selected mode for the user.
display.scroll(function.upper())
radio.on() # Radio needs to be turned on. Or radio functions will throw exception.


# Main loop

while(True):

    if(function == "sender"):

        # Simple selection, left or right.
        if(button_b.was_pressed() and user_selection < len(images)-1):
            user_selection += 1
        elif(button_a.was_pressed() and user_selection > 0):
            user_selection -= 1

        display.show(images[user_selection])

        # If the selection changes, and the image has not been sent.
        if(user_selection != last_sent):
            radio.send(str(user_selection)) # Function can only send strings.
            last_sent = user_selection  # Updating the last sent image.
    else:
        incoming = radio.receive() # Receives input
        if(incoming):
            if(int(incoming) in range(0,len(images))):
                # Selection is valid and displays corresponding image.
                display.show(images[int(incoming)])
