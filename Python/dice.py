"""

Example: dice.py

Demonstrates:
 - Accelerometer
 - Display
 - Images
 - Gestures

Shake the device like a dice

"""

from microbit import accelerometer, Image, display, sleep
from random import randrange


# Dice sides:

dice = [
Image("00000:"
"00000:"
"00900:"
"00000:"
"00000:"),
Image("00009:"
"00000:"
"00000:"
"00000:"
"90000:"),
Image("00009:"
"00000:"
"00900:"
"00000:"
"90000:"),
Image("90009:"
"00000:"
"00000:"
"00000:"
"90009:"),
Image("90009:"
"00000:"
"00900:"
"00000:"
"90009:"),
Image("90009:"
"00000:"
"90009:"
"00000:"
"90009:"),
]

while True:

    if accelerometer.was_gesture("shake"):
        dice_side = randrange(0,6)
        display.show(dice, delay=150)
        display.clear()
        display.show(dice[dice_side])
