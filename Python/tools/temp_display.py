try:
    import serial
    import sys
    from serial.tools.list_ports import grep as find_port
    import curses
    from requests import get as push_data
    from time import strftime
    from pyfiglet import Figlet

except ImportError as e:
    print "Error while importing modules: %s" % e.message
    exit(1)

VID = hex(3368).replace("0x","0")
PID = hex(516).replace("0x","0")

try:
    port = find_port("^USB VID:PID=%s:%s" % (VID, PID)).next()
except StopIteration:
    print "Error while connection to device: Device not found."
    exit(1)

dev = serial.Serial(
    port=port.device,
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,

)

last_update = -100

# Variables for sparkfun

data_id = "EJ4QVNVAJvt2bA38MpR0"
data_priv_key = "dqnRKbKmq0TwGBZ64lKk"

# Figlet stuff

figlet = Figlet(font="doh")

def pad_to_size(text, x, y):
    """
    Adds whitespace to text to center it within a frame of the given
    dimensions.

    Code borrowed, adapted and stolen from https://github.com/trehn/termdown/
    """
    input_lines = text.rstrip().split("\n")
    longest_input_line = max(map(len, input_lines))
    number_of_input_lines = len(input_lines)
    x = max(x, longest_input_line)
    y = max(y, number_of_input_lines)
    output = ""

    padding_top = int((y - number_of_input_lines) / 2)
    padding_bottom = y - number_of_input_lines - padding_top
    padding_left = int((x - longest_input_line) / 2)

    output += padding_top * (" " * x + "\n")
    for line in input_lines:
        output += padding_left * " " + line + " " * (x - padding_left - len(line)) + "\n"
    output += padding_bottom * (" " * x + "\n")

    return output


def draw_text(stdscr, text):
    """
    Draws text in the given color. Duh.

    Code borrowed, adapted and stolen from https://github.com/trehn/termdown/
    """
    y, x = stdscr.getmaxyx()
    lines = pad_to_size(text, x, y).rstrip("\n").split("\n")

    try:
        for i, line in enumerate(lines):
            stdscr.insstr(i, 0, line)
    except:
        pass
    stdscr.refresh()

if not dev.isOpen():
    dev.open()
try:
    stdscr = curses.initscr()

    curses.noecho()    # don't echo the keys on the screen
    curses.cbreak()    # don't wait enter for input
    curses.curs_set(0) # don't show cursor.
    height, width = stdscr.getmaxyx()

    # Set up Figlet

    figlet.width = width

    stdscr.addstr(height/2, width/2-4, "Waiting!")
    stdscr.refresh()

    while True:
        temperature = dev.read(2)
        if temperature != last_update:
            big_text = figlet.renderText(str(temperature))
            draw_text(stdscr, big_text)
            curses.flash()
            # stdscr.addstr(height/2, width/2, temperature)
            url_data = "http://data.sparkfun.com/input/%s?private_key=%s&time=%s&temperature=%s" % \
             (data_id, data_priv_key, strftime("%H:%M%%20%d.%m.%Y"), str(temperature))
            result = push_data(url_data)
            last_update = temperature

except KeyboardInterrupt:
    print "Bye"
except:
    curses.endwin()
    print "Error: %s" % sys.exc_info()[0]
finally:
    curses.endwin()